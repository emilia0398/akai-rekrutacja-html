/*
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    AKAI Frontend Task - Javascript

    W tym zadaniu postaraj się zaimplementować metody, które sprawdzą, czy dane wprowadzone
    do formularza są poprawne. Przykładowo: czy imię i nazwisko zostało wprowadzone.
    Możesz rozwinąć walidację danych tak bardzo, jak tylko zapragniesz.

    Powodzenia!
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

function validateForm() {
   
    var firstName = document.getElementById("first-name").value;
    var lastName = document.getElementById("last-name").value;
    var email = document.getElementById("email").value;

    var checkBox1 = document.getElementById("frontend-checkbox").checked;
    var checkBox2 = document.getElementById("backend-checkbox").checked;
    var checkBox3 = document.getElementById("mobile-checkbox").checked;
    var checkBox4 = document.getElementById("graphics-checkbox").checked;

    var letters = /^[a-zA-Z]+$/;
    var checkName = true;
    var checkLastName = true;


    if (firstName == "") {
        alert("Proszę wpisać imię");   
    }
    else if (firstName.match(letters)) {
        checkName = true;
    }
    else {
        alert('Niepoprawne imię');
        checkName = false;
    }

    
    if (lastName == "") {

        alert("Proszę wpisać nazwisko");   
    }
    else if (lastName.match(letters)) {
        checkLastName = true;
    }
    else {
        alert('Niepoprawne nazwisko');
        checkLastName = false;
    }

    if (email == "") {

        alert("Proszę wpisać adres e-mail");
    }

    if (checkBox1 == false && checkBox2 == false && checkBox3 == false && checkBox4 == false) {

        alert("Proszę wybrać sekcję");
        return false;
    }

    if ( firstName=="" || lastName=="" || email=="" )
    {
        return false;
    }
    else if (checkName==false || checkLastName == false)
    {
        return false;
    }
}
